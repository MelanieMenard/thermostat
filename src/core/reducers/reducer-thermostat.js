/* ------------------------------------------- */
/*   thermostat Reducer
/* ------------------------------------------- */

import { createReducer } from 'AppSrc/core/reducers/create-reducer';
import {
  SET_SETPOINT_REQUEST,
  SET_SETPOINT_SUCCESS,
  SET_SETPOINT_ERROR,
} from 'AppSrc/core/actions/actions-thermostat';

const thermostatReducerDefaultState = {
  isSaving: false,
};

const setSetpointRequest = (state, action) => ({
  ...state,
  isSaving: true,
});

const setSetpointSuccess = (state, action) => ({
  ...state,
  isSaving: false,
});

const setSetpointError = (state, action) => ({
  ...state,
  isSaving: false,
});



// Action handlers dictionary
// defines the mapping between actions and the handler methods
const actionHandlers = {
  [SET_SETPOINT_REQUEST]: setSetpointRequest,
  [SET_SETPOINT_SUCCESS]: setSetpointSuccess,
  [SET_SETPOINT_ERROR]: setSetpointError,
};

// create the reducer
const thermostatReducer = createReducer(thermostatReducerDefaultState, actionHandlers);

export {
  thermostatReducerDefaultState,
  thermostatReducer,
};
