/* ------------------------------------------- */
/*   Redux Root Reducer
/*   The Root reducer combines the different reducers responsible for updating specific bits of data in the state
/* ------------------------------------------- */

import { combineReducers } from 'redux';
import { errorReducer } from 'AppSrc/core/reducers/reducer-error';
import { thermostatReducer } from 'AppSrc/core/reducers/reducer-thermostat';


const rootReducer = combineReducers({
  error: errorReducer,
  thermostat: thermostatReducer,
});

export { rootReducer };
