/** Utility function to generate a reducer based on an object mapping action types to handler functions * */
/* Used instead of a switch case when each case would be too complex
/* (typically requires const declarations, which eslint forbids in case branches)
/*  Based on:
/*  https://redux.js.org/recipes/reducing-boilerplate#generating-reducers
 * */
// defaultState: the defined default state used to initalise the slice of state
// handlers: An object whose keys are the actions types to respond to, and whose values are functions that return the state based on those actions.
function createReducer(defaultState, handlers) {
  // return a reducer function based on the parameters
  return function reducer(state = defaultState, action) {
    // if the current action is contained as a key in the incoming handlers object
    if (handlers.hasOwnProperty(action.type)) {
      // pass the state an action to the handler function
      return handlers[action.type](state, action);
    }
    // if no handler has the action.type as key, return the state untouched
    return state;

  };
}

export { createReducer };
