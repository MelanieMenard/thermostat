/* ------------------------------------------- */
/*   Actions for thermostat API
/* ------------------------------------------- */

import {
  postThermostatAction,
} from 'AppSrc/core/queries/queries-thermostat';


/* --- Actions Types --- */

const SET_SETPOINT_REQUEST = 'SET_SETPOINT_REQUEST';
const SET_SETPOINT_SUCCESS = 'SET_SETPOINT_SUCCESS';
const SET_SETPOINT_ERROR = 'SET_SETPOINT_ERROR';


/* ------ ACTION CREATORS ------ */

const setSetpointRequest = (value) => ({
  type: SET_SETPOINT_REQUEST,
  payload: {
    value,
  },
});

const setSetpointSuccess = (value) => ({
  type: SET_SETPOINT_SUCCESS,
  payload: {
    value,
  },
});

const setSetpointError = (error) => ({
  type: SET_SETPOINT_ERROR,
  payload: {
    error,
  },
});



/* - thunk action  - */
const setSetpoint = (value) => (dispatch, getState) => {

  // notify reducer saving has started
  dispatch(setSetpointRequest(value));

  // REST POST request
  return postThermostatAction('setSetPoint', value)
    .then((data) => {
      console.log('postThermostatAction SUCCESS: ', data);
      dispatch(setSetpointSuccess(data));
    })
    .catch((error) => {
      console.log('postThermostatAction ERROR: ', error);
      dispatch(setSetpoint(error));
    });
};


export {
  SET_SETPOINT_REQUEST,
  SET_SETPOINT_SUCCESS,
  SET_SETPOINT_ERROR,
  setSetpointRequest,
  setSetpointSuccess,
  setSetpointError,
  setSetpoint,
};
