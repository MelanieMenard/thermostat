/* ------------------------------------------- */
/*   Axios instance that reused accross queries for efficiency
/*   Generic REST query functions making use of the instance, used in the async thunk actions
/* ------------------------------------------- */

import axios from 'axios';
import http from 'http';
import https from 'https';
import {
  catchHiddenError,
  handleError,
} from 'AppSrc/core/queries/error-handler';

// First you set global config defaults that will be applied to every instances and requests
// https://github.com/axios/axios#global-axios-defaults
// 50 sec timeout
axios.defaults.timeout = 50000;
// follow up to 10 HTTP 3xx redirects
axios.defaults.maxRedirects = 10;
// cap the maximum content length we'll accept to 50MBs, just in case
axios.defaults.maxContentLength = 50 * 1000 * 1000;
// keepAlive pools and reuses TCP connections, so it's faster
axios.defaults.httpAgent = new http.Agent({ keepAlive: true });
axios.defaults.httpsAgent = new https.Agent({ keepAlive: true });


// then you create instance specific config when creating the instance
// https://github.com/axios/axios#custom-instance-defaults
// if we had authorization headers we would set them here
const axiosInstance = axios.create();


// utility function for posting a REST GET query to axios
// includes handling of 200 responses with hidden errors
// and standard error responses
// returns a promise with the response
// for REST queries, the endpoint depends on the data, so getRESTQuery gets it as an argument along with the query
// so this utility function is reusable for any REST data
const getRESTQuery = (endpoint, query) => axiosInstance.get(endpoint, { params: query })
  .then((response) => catchHiddenError(response))
  .catch((error) => handleError(error));

// utility function for posting a REST POST query to axios
// MM: some POST request need more config than just the query for post request as opposed to get requests, for example content-type on header
const postRESTQuery = (endpoint, data, config) => axiosInstance.post(endpoint, data, config)
  .then((response) => response)
  .catch((error) => handleError(error));


export {
  axiosInstance,
  getRESTQuery,
  postRESTQuery,
};

