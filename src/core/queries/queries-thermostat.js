/* ------------------------------------------- */
/*   Queries for thermostat API
/* ------------------------------------------- */

import {
  postRESTQuery,
} from 'AppSrc/core/queries/axios-queries';

const apiBaseUrl = 'https://dev.edflabs.net/thermostat/web/api';


const postThermostatAction = (action, value) => {

  const endpoint = `${apiBaseUrl}/request}`;

  const data = {
    action,
    value,
  };

  return postRESTQuery(endpoint, data)
    .then((response) => {
      console.log('postThermostatAction RESPONSE: ', response);
      return response.data;
    });
};


export {
  postThermostatAction,
};
