import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setSetpoint } from 'AppSrc/core/actions/actions-thermostat';


/* ---  presentational component --- */

class SmartThermostatDisplay extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      setPoint: 20,
    };
    this.onSetSetPointClicked = this.onSetSetPointClicked.bind(this);
    this.onNumericInputChange = this.onNumericInputChange.bind(this);
  }

  onNumericInputChange(e) {
    const stateKey = e.target.name;
    // the number input returns a string
    const value = parseInt(e.target.value);
    // input can temporarily lead to NaN when the user is typing (empty box, minus sign)
    // in that case don't update the controlled input
    if (Number.isNaN(value)) {
      return;
    }
    this.setState({
      [stateKey]: value,
    });
  }

  onSetSetPointClicked() {
    this.props.setThermostatSetpoint(this.state.setPoint);
  }

  render() {
    const {
      setPoint,
    } = this.state;

    return (
      <div className="thermostat">
        <label htmlFor="setPoint">
          setPoint:
          <input type="number" id="setPoint" name="setPoint" value={setPoint} onChange={this.onNumericInputChange} />
        </label>
        <button
          type="button"
          className="set-setPoint"
          onClick={this.onSetSetPointClicked}
        >
          SET SETPOINT
        </button>
      </div>
    );
  }
}


SmartThermostatDisplay.propTypes = {
  setThermostatSetpoint: PropTypes.func,
};


/* --- container component --- */

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch, ownProps) => ({
  setThermostatSetpoint: (value) => {
    console.log('setThermostatSetpoint: ', value);
    dispatch(setSetpoint(value));
  },
});

const SmartThermostat = connect(
  null,
  mapDispatchToProps,
)(SmartThermostatDisplay);


// 'Display' unconnected presentational components are exported for testing only
export {
  SmartThermostatDisplay,
  SmartThermostat,
};

