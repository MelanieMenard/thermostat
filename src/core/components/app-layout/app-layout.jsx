import React from 'react';
import './app-layout.css';
import { SmartThermostat } from 'AppSrc/core/components/smart-thermostat/smart-thermostat';


/* ---  presentational component --- */

const AppLayout = () => (
  <div className="app">

    <header className="app-header">
      Smart Thermostat
    </header>

    <div className="app-content">
      <SmartThermostat />
    </div>

  </div>
);


export {
  AppLayout,
};
